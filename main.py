""" 
@file main.py
@brief This module instantiates all other tasks and runs them using a priority scheduler.
@details Each task is imported an instantiated as a cotask.Task object so that cotask.task_list
can run each task at different intervals using a priority scheduler. For more information on
each task and how they communicate refer to @ref mainPage "ME405 Final Project".
\n
<B>source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/main.py
@author Eliot Briefer and Nick Greco
@date 6/11/2021
"""
#System imports
import pyb
from micropython import alloc_emergency_exception_buf
import gc

#Scheduler imports
import cotask

#Task imports
from ME405_FP_Shares import programExitShare, enabledShare
from ME405_FP_EncoderTask import encoderTaskFun
from ME405_FP_TouchPanelTask import touchPanelTaskFun
from ME405_FP_ControllerTask import controllerTaskFun
from ME405_FP_MotorTask import motorTaskFun
from ME405_FP_UserTask import userInterfaceFun
import ME405_FP_DataTask
from ME405_FP_DataTask import dataCollectionTask, closeCSV

# Allocate memory so that exceptions raised in interrupt service routines can
# generate useful diagnostic printouts
alloc_emergency_exception_buf (100)

# =============================================================================

if __name__ == "__main__":

    print ('\033[2JTesting scheduler in cotask.py\n')

    # Create the tasks. If trace is enabled for any task, memory will be
    # allocated for state transition tracing, and the application will run out
    # of memory after a while and quit. Therefore, use tracing only for 
    # debugging and set trace to False when it's not needed
    
    ##@brief The cotask.Task object used to run the encoder task.
    taskENC = cotask.Task(encoderTaskFun, name = 'Encoder_Task', priority = 3, 
                          period = 20, profile = True, trace = False)
    ##@brief The cotask.Task object used to run the @ref ME405_FP_TouchPanelTask.py "touch panel task".
    taskTP = cotask.Task(touchPanelTaskFun, name = 'TouchPanel_Task', priority = 3, 
                        period = 20, profile = True, trace = False)
    ##@brief The cotask.Task object used to run the @ref ME405_FP_ControllerTask.py "controller task".
    taskC = cotask.Task(controllerTaskFun, name = 'Controller_Task', priority = 3,
                          period = 20, profile = True, trace = False)
    ##@brief The cotask.Task object used to run the @ref ME405_FP_MotorTask.py "motor task".
    taskMOT = cotask.Task(motorTaskFun, name = 'Motor_Task', priority = 3,
                          period = 20, profile = True, trace = False)
    ##@brief The cotask.Task object used to run the @ref ME405_FP_UserTask.py "user interface task".
    taskUI = cotask.Task(userInterfaceFun, name = 'UI_Task', priority = 1,
                          period = 10, profile = True, trace = False)
    ##@brief The cotask.Task object used to run the @ref ME405_FP_DataTask.py "data collection task".
    taskDATA = cotask.Task(dataCollectionTask, name = 'Data_Task', priority = 0,
                            period = 15, profile = True, trace = False)
    
    # Append a task to the task list. The list will be sorted by task priorities 
    # so that the scheduler can quickly find the highest priority task which is 
    # ready to run at any given time.
    cotask.task_list.append(taskENC)
    cotask.task_list.append(taskTP)
    cotask.task_list.append(taskC)
    cotask.task_list.append(taskMOT)
    cotask.task_list.append(taskUI)
    cotask.task_list.append(taskDATA)

    # Run the memory garbage collector to ensure memory is as defragmented as
    # possible before the real-time scheduler is started
    gc.collect ()

    # Run the scheduler using priority scheduling
    vcp = pyb.USB_VCP ()
    while programExitShare.get() == 0:
        cotask.task_list.pri_sched()
        
    # Disable all tasks, then run the scheduler another 100 times to insure that
    #all tasks disable. This is necessary to insure that the motors disable before
    #the program exits, otherwise they will continue to spin until the board is
    #unpluged or the program is restarted.
    for i in range(100):
        enabledShare.put(0)
        cotask.task_list.pri_sched()
        
    # Empty the comm port buffer of the character(s) just pressed
    vcp.read ()
    closeCSV() #Imported from data collection task, closes CSV used for data logging

