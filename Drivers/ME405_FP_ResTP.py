"""@file    ME405_FP_ResTP.py

@brief      Sets up touch panel scan in x-, y-, and z-directions, and returns results in a tuple
@details    Using a pair of resistive wires, voltages are read at specific instances using ADC
            methodology. By using these values and performing appropriate calibration tests,
            the touch panel allows for accurate representation of where an object is with respect 
            to its physical whereabouts, and it can tell if an object is indeed on its surface.
            
            <B>source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/Drivers/ME405_FP_ResTP.py
@author     Nicholas Greco
@date       6/11/2021
"""

# configuring pins
import pyb
import utime
from pyb import ADC, Pin
from array import array


class ResTP:
    '''@brief Runs operations and returns values specific to the encoder count
       @details Class for updating, getting, setting, and computing the change 
                in the encoder count.
    '''
    pin0 = None 
    pin1 = None
    pin6 = None
    pin7 = None
    
    l_p = 0 
    w_p = 0
    cntr_p = [0, 0] # [x-pos, y-pos]
    splyVlt = 3.3
    
    def __init__(self, pin0, pin1, pin6, pin7):
        ''' @brief Assign a pin to each Cartesian coordinate variable (one per sign)
            @details The initialization also includes calibrating the touch panel
                     so that results start from (0,0), and priming the pins so that they
                     read in ADC mode, which will allow us to interpret voltage differences
                     that indicate the presence of an object
        '''
        self.pin0 = pin0   # The y_m pin
        self.pin1 = pin1   # The x_m pin
        self.pin6 = pin6   # The y_p pin
        self.pin7 = pin7   # The x_p pin
        
        self.l_p = 176  # units of mm
        self.w_p = 100 # units of mm

        # confirm where the touch panel considers zero to be
        self.cntr_p = [self.l_p*(1/2), self.w_p*(1/2)] 
        
        # Be sure to set pin A0 (the "y_m" pin) as the pin to collect ADC data
        ## @brief Sets the pin to ADC mode and stores the values in the variable "adc"
        # self.adc_XZ = pyb.ADC(self.pin0) # instantiatiing the ADC class with an object
        # self.adc_Y = pyb.ADC(self.pin1)
        
        # Calibration results
        self.x_calib = -1.4
        self.y_calib = -3.5
        
        self.x_scale = 1.2
        self.y_scale = 1.3
        
    def x_scan(self):
        ''' @brief Relays positional information in the x-direction
            @details By detecting voltage changes (via ADC method) brought about 
                     by the contact of two paths of wires oriented vertically 
                     to one another, the position of an object on the platform in
                     the x-direction can be detected.
            @return x_pos*self.x_scale  The x-position scaled to place (0,0) at the platforms center
        '''
        self.pin7.init(mode = Pin.OUT_PP, value = 1) # Setting pin associated with x_p high
        self.pin1.init(mode = Pin.OUT_PP, value = 0) # Setting pin associated with x_m low
        self.pin6.init(mode = Pin.IN) # Float yp
        # self.pin0.init(mode = pyb.Pin.IN, pull = pyb.Pin.PULL_NONE) # Float ym
        
        adc_XZ = ADC(self.pin0) # setting y_m as ADC pin
        # Read ADC value from pin0 
        Vx_cnts = adc_XZ.read()
        #print("Voltage in Counts {}".format(Vx_cnts))
        # Convert "x"-position voltage to counts
        Vx_volts = (Vx_cnts/4095)*self.splyVlt
        
        # Formula to turn Voltage due to pressure and system parameters into counts
        x_pos = ((Vx_volts/self.splyVlt)*self.l_p) - (self.cntr_p[0] + self.x_calib)
        #print("xPos_TP {}".format(x_pos))
        return x_pos*self.x_scale 
    # Tune the x_scan (not properly working)
        
        
    def y_scan(self):
        ''' @brief Relays positional information in the y-direction
            @details Delivers y-position information of a object on the touch 
                    panel using an identical conversion of voltage to count via 
                    an ADC pin as the x-scan method.
            @return y_pos*self.y_scale  The y-position scaled to place (0,0) at the platforms center
        '''
        self.pin6.init(mode = pyb.Pin.OUT_PP, value = 0) # Setting pin associated with y_p high
        self.pin0.init(mode = pyb.Pin.OUT_PP, value = 1) # Setting pin associated with y_m low
        self.pin7.init(mode = pyb.Pin.IN, pull = pyb.Pin.PULL_NONE) # Float xp
        # self.pin1.init(mode = pyb.Pin.IN, pull = pyb.Pin.PULL_NONE) # Float xm
        
        # Must be included in the code because of possible ADC bug; cannot just instantiate
        # object in the constructor
        adc_Y = pyb.ADC(self.pin1) # set x_m as ADC pin
        Vy_cnts = adc_Y.read()
        # print(Vy_cnts)
        # Convert "x"-position voltage to counts
        Vy_volts = (Vy_cnts/4095)*self.splyVlt
        # Formula to turn Voltage due to pressure and system parameters into counts
        y_pos = ((Vy_volts/self.splyVlt)*self.w_p) - (self.cntr_p[1] + self.y_calib)
        return y_pos*self.y_scale
        
    
    def z_scan(self):
        ''' @brief Detects whether or not an object is in contact with the platform.
            @details Uses the voltage to count conversion via ADC, but employs it to ensure
                     there is contact with the touch panel. The panel is set to read high,
                     therefore when a count value is detected that is below the max count
                     multiplied by some FOS, this implies that contact with the panel
                     is present.
            @return A boolean True/False to indicate whether there is contact with the panel
        '''
        self.pin6.init(mode = pyb.Pin.OUT_PP, value = 1) # Setting pin associated with y_p high
        self.pin1.init(mode = pyb.Pin.OUT_PP, value = 0) # Setting pin associated with x_m low
        self.pin7.init(mode = pyb.Pin.IN, pull = pyb.Pin.PULL_NONE) # Float xp
        
        # can't do pin.OUT_PP without using the init function
        # self.pin6([True]) 
        # self.pin1([False])
        
        # Be sure to set pin A0 (the "y_m" pin) as the pin to collect ADC data
        ## @brief Sets the pin to ADC mode and stores the values in the variable "adc"
        
        adc_XZ = pyb.ADC(self.pin0) # setting y_m as ADC pin
        
        cnt_Val = adc_XZ.read() # reading from the ADC object
        return cnt_Val < 0.90*4095 # returns True when pressed
    
    def runScan(self):
        ''' @brief Runs the scan methods and stores the readings in a tuple
            @return Tuple_ScanVal The value of the x-, y-, and z-scans as quickly as the touch panel cycles
        '''
        # assuming scan will read left to right, just as it would top down
        Tuple_ScanVal = (self.x_scan(), self.y_scan(), self.z_scan())
        return Tuple_ScanVal
 
 
if __name__ == "__main__":
    pinA0 = pyb.Pin(pyb.Pin.cpu.A0) # ym of the touch panel
    pinA1 = pyb.Pin(pyb.Pin.cpu.A1) # xm of the touch panel
    pinA6 = pyb.Pin(pyb.Pin.cpu.A6) # yp of the touch panel
    pinA7 = pyb.Pin(pyb.Pin.cpu.A7) # xp of the touch panel


    tchPanel = ResTP(pinA0, pinA1, pinA6, pinA7)
    while True:
        scanRdg = tchPanel.runScan()
        # startTime = utime.ticks_us()
        # for _ in range(100):
        #     tchPanel.runScan()
        #     stopTime = utime.ticks_us()
        
        # timePerRun = (stopTime - startTime)/100
        # print(timePerRun)   
        
        # print("xScanVal: {}, yScanVal: {}, zScanVal: {}".format(scanRdg[0], scanRdg[1], scanRdg[2]))
        utime.sleep(1)
        