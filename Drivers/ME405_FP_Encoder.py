# -*- coding: utf-8 -*-
"""@file    ME405_FP_Encoder.py

@brief      Implements the onboard encoder to count the motion of the motor arm
@details    The encoder is connected to the motor in such a way as to be able
            to monitor the position of the motor arm whenever the encoder
            samples the data. 
            \n
            <B> source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/Drivers/ME405_FP_Encoder.py
            
            <B>source code:</B> 
@author     Nicholas Greco
@date       6/11/2021
"""

import pyb
import utime
from array import array
from math import pi

class ME405_FP_Encoder:
    '''@brief Runs operations and returns values specific to the encoder count
       @details Class for updating, getting, setting, and computing the change 
                in the encoder count.
    '''

    # Encoder Driver class that will later be implemented in tasks
    # Static variables are defined outside of methods
    

    def __init__(self, tim, pin1, pin2):
        ''' @brief Initiates encoder operations using approprate timers and pins.
            @details The encoder architecture requires two channels 
                     from which the timer reads in order to determine directional 
                     differences
        '''
        self.AngList = []
        self.DatumCnt = 0
        self.DeltaCnt = 0
        self.theta = 0
        ## @brief The timer from which the encoder counts
        self.tim = tim
        
        ## @brief One of two pins needed for collecting data on motor ops
        self.pin1 = pin1
        ## @brief One of two channels needed for the encoder operation
        self.timCh1 = self.tim.channel(1, pyb.Timer.ENC_A, pin=self.pin1)
        
        ## @brief One of two pins needed for collecting data on motor ops
        self.pin2 = pin2
        ## @brief One of two channels needed for the encoder operation
        self.timCh2 = self.tim.channel(2, pyb.Timer.ENC_B, pin=self.pin2)
        
        ## @brief Initial value that will be used to measure a change in count
        self.DatumCnt = self.tim.counter()
        
    def update(self):
        '''@brief Updates the encoder count and corrects for overflow
           @return theta The position of the encoder arm in ticks
        '''

        ## @brief A count value that will be updated each time update is called.
        #  @details A change in value for newCnt, or for that matter any value based
        #           on timer counts, will only occur if the arm of the motor is
        #           rotated, because the rotation of the motor arm is what brings
        #           about the change in pulses of the encoder, which is interpreted
        #           by the timer attached to the encoder channel and returns a
        #           change in count to reflect the change in the motor arm position
        self.newCnt = self.tim.counter()
        #print("New Count: {}".format(self.newCnt))
        
        # The class is constructed 1st, meaning that DatumCnt will be less than
        # newCnt by definition. The difference between newCnt and DatumCnt will 
        # continue to grow the longer the period between which the update 
        # method is called. This will lead to an overflow problem that needs 
        # resolved.
        ## @brief Establishes the change in count
        #  @details The change in count represents the change in the location
        #           of the motor arm between sample periods. This change in 
        #           count conveys direction and velocity data. The count can 
        #           only go so high before an overflow condtion occurs, meaning
        #           we have to correct the overflow in order to get a 
        #           continuous representation of the motor;s motion.
        self.DeltaCnt = self.newCnt - self.DatumCnt
        #print("Datum Count: {}".format(self.DatumCnt))
        
        # Corrects the overflow problem
        if self.DeltaCnt < 0xFFFF/2 and self.DeltaCnt > -1*(0xFFFF/2):
            pass
        elif self.DeltaCnt > 0xFFFF/2:
            self.DeltaCnt = self.DeltaCnt - 0xFFFF
        elif self.DeltaCnt < -1*(0xFFFF/2):
            self.DeltaCnt = self.DeltaCnt + 0xFFFF
            
        # This allows us to always be measuring change in count (hence the 
        # delta in DeltaCnt)
        self.DatumCnt = self.newCnt
        # Theta, which will eventually represent the angle of the motor arm, 
        # changes as the count changes, because the changing count represents
        # the measured rotation of the motor arm by the encoder
        self.theta += self.DeltaCnt
        #print("theta: {}".format(self.theta))
            
        return self.theta * (2*pi)/4000
        # Unsure how to convert from delta, measured by value of the count, to
        # theta, which corresponds to the angle of the motor arm
        
    def getPosition(self):
        '''@brief Gives the last stored position of the encoder arm
           @return theta; The position of the encoder arm in ticks
        '''
        return self.theta
        # alternatively, can use "-1" in place of len(self.AngList) to get the
        # last element of the list
    
    def setPosition(self, reset_value):
        '''@brief Sets the encoder count to a desired value
           @param reset_value; The position one wishes to set the encoder to
        '''
        self.DeltaCnt = reset_value
    
    def getDelta(self):
        '''@brief Returns the change between the last two counts in update
           @return DeltaCnt; The change in the count 
        '''
        #print("Change in Count: {}".format(self.DeltaCnt))
        return self.DeltaCnt
        
    
    

        
        
                