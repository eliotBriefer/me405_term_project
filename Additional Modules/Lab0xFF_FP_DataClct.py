# -*- coding: utf-8 -*-
"""
Created on Tue Jun  8 15:25:28 2021

@author: nicho
"""
from ME405_FP_EncoderTask import t_sample, thetaXQueue, thetaXDotQueue, thetaYQueue, thetaYDotQueue, xPos_Q, xDot_Q, yPos_Q, yDot_Q, dataClct
import utime

fileName = 'Platform_Motion.csv'
BalanceData_file = open('Platform_Motion.csv','w')

def Write2CSV():
    ''' @brief Writes to the established file upon a particular flag raise from the UserTask
    '''
    while True: 
        if dataClct.get() == 1:      
            encTimeQueue.get()
            # The means of opening a file and appending data to the CSV file 
            BalanceData_file.write("{:},{:},{:},{:},{:},{:},{:},{:}\r\n".format(thetaXQueue, thetaXDotQueue, thetaYQueue, thetaYDotQueue, xPos_Q, xDot_Q, yPos_Q, yDot_Q))
    yield(0)

def closeCSV():
    ''' @brief Closes the CSV file, preventing further data from being written
    '''
    BalanceData_file.close()

                
             
        
                    



