'''
@file ME405_FP_FSC.py
@brief This module contains the FSC class which is a generalised full state controller
@details See https://en.wikipedia.org/wiki/Full_state_feedback for more details on full state control.
Note that FSC does not rely on any micropython specific libraries and could be used on any hardware
capable of running standard python 3.
\n
<B>source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/Additional%20Modules/ME405_FP_FSC.py
@author Eliot Briefer
@date 6/11/2021
'''
class FSC:
    '''
    @brief The FSC class uses a state space representation of a system to calculate a SINGLE output given a set of gains and a current state.
    @details To control a multiple output system an FSC object will need to be created for each output.
    '''
    def __init__(self, numStateVariables, gains = None):
        '''
        @brief A mehtod to initialise an FSC object.
        @param numStateVariables The number of state variables used to represent the system (integer).
        @param gains An itterable (such as list, array, tuple, etc.) containing the numerical gains of the controler. If no gains are passed in gains will default to zero.
        '''
        self.numStateVars = numStateVariables
        self.setGains(gains)
            
    def update(self, state):
        '''
        @brief A method used to calculate the output based on a new state.
        @details The state vector must be the same length as the number of states.
        @param state An itterable (such as list, array, tuple, etc.) containing the numerical state vector of the system.
        @return The calculated output.
        '''
        output = 0
        if len(state) == self.numStateVars:
            for i, s in enumerate(state): 
                output += self.gains[i] * s
        else:
            raise ValueError('Incorrect number of state variables.')
        # print("State: {}".format(state))
        # print("Gains: {}".format(self.gains))
        # print(output)
        return output
        
    def setGains(self, gains):
        '''
        @brief A method to change the controller gains
        @param gains An itterable (such as list, array, tuple, etc.) containing the new numerical gains of the controler. If no gains are passed in gains will default to zero.
        '''
        if gains is not None:
            if len(gains) == self.numStateVars:
                self.gains = gains
            else:
                raise ValueError('Gains must have a 1 to 1 correspondence with state variables.')
        else:
            self.gains = [0 for i in range(self.numStateVars)]

if __name__ == "__main__":
    #TEST CODE
    #test initialize
    testFSC = FSC(4, [0, 1, 2, 3])
    print('Gains should be [0, 1, 2, 3]:')
    print(testFSC.gains)
    
    #test FSC.setGains()
    testFSC.setGains([5, 4, 3, 2])
    print('Gains should be [5, 4, 3, 2]:')
    print(testFSC.gains)
    
    #test FSC.update()
    state = [1, 1, 1, 1]
    print('State: ' + str(state))
    output = testFSC.update(state)
    print('output should be 5*1+4*1+3*1+2*1 = 14.')
    print(output)
    state = [0, 0, 5, 0]
    print('State: ' + str(state))
    print('output should be 5*0+4*0+3*5+2*0 = 15.')
    output = testFSC.update(state)
    print(output)
    
    state = [-1, -1, -1, -1]
    print('State: ' + str(state))
    output = testFSC.update(state)
    print('output should be 5*1+4*1+3*1+2*1 = 14.')
    print(output)