'''
@file ME405_FP_ABFilter.py
@brief An alpha beta filter for more accurate position and velocity measurments from granular data.
@details For more information on alpha beta filters see: https://en.wikipedia.org/wiki/Alpha_beta_filter
\n
<B>source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/Additional%20Modules/ME405_FP_ABFilter.py
'''
try: #To allow for testing on PC
    import utime
except:
    print('utime import failed')

class ABFilter:
    '''
    @brief A class implementation of an alpha beta filter for position and velocity estimation.
    @details Alpha beta filters are usefull for more acuratley estimating position and velocity
    from granular readings, such as a slow spinning encoder or a one pole hall efect rpm sensor.
    @author Eliot Briefer
    '''
    
    def __init__(self, alpha=0.85, beta=0.005, prescaler = 10**6, x_not = 0, v_not = 0, t_not = 0, useUtime = True):
        '''
        @brief A method to initialize an ABFilter.
        @param alpha The alpha value
        @param beta The beta value
        @param prescaler Used to correct velocity into units/sec. Prescaler value is 
        @param x_not Initial position
        @param v_not Initial velocity
        @param t_not Initial tick (typically in micro seconds)
        @param useUtime Wether or not to use utime.ticks_diff(). 
        Set to false if time ticks arent generated using utime.ticks_ms() or utime.ticks_us().
        '''
        ## @brief The alpha value used for velocity estimation
        self.alpha = alpha
        ##@brief The beta value used for velocity estimation
        self.beta = beta
        ##@brief The most recent filtered x position
        self.x = x_not
        ##@brief The most recent filtered y position
        self.v = v_not
        ##@brief The most recent time tick, typically in microseconds
        self.t = t_not #typically in microseconds
        ##@brief wether or not time ticks are generated with utime
        self.useUtime = useUtime
        ##@brief The prescaler to multiply the velocity by to get x units per second
        self.prescaler = prescaler
        
    def update(self, x_new, t_new):
        '''
        @brief Used to update estimate a new velocity and position based on a measured position and time.
        @param x_new new x reading
        @param t_new new t tick, typically in us from utime.ticks_us()
        @return A tuple of (estimated position, estimated velocity)
        '''
        #estimate velocity based on new data point
        #returns a tuple of (est pos, est vel)
        #velocity is in units of (x units)/(t tick units)
        #adapted from example code on https://en.wikipedia.org/wiki/Alpha_beta_filter
        dt = None
        if self.useUtime:
            dt = utime.ticks_diff(t_new, self.t)
            pass
        else:
            dt = t_new - self.t
        x_est = self.x + (self.v * dt)
        v_est = self.v
        
        r = x_new - x_est
        
        x_est += self.alpha * r
        if dt != 0: #avoid division by zero error
            v_est += (self.beta * r) / dt
        
        self.x = x_est
        self.v = v_est
        self.t = t_new
        
        return (self.x, self.v * self.prescaler)
    
    def setAB(self, alpha = None, beta = None):
        '''
        @brief A method to change the alpha and beta values used for position and velocity estimation
        @param alpha The new alpha value, should be < 1.
        @param beta The new beta value, should be small, typically < 0.1
        '''
        #change alpha and beta values
        if alpha is not None:
            self.alpha = alpha
        if beta is not None:
            self.beta = beta
    
    def setState(self, x = None, v = None, t = None):
        '''
        @brief A method to reset the ABFilter to a specified position, velocity, and time.
        @param x The set position.
        @param v The set velocity.
        @param t The set time tick.
        '''
        #set current position, velocity, and time
        if x is not None:
            self.x = x
        if v is not None:
            self.v = v
        if t is not None:
            self.t = t
            
if __name__ == "__main__":
    #Testing code for ABFilter class
    import matplotlib.pyplot as plt
    from math import sin
    
    abTest = ABFilter(0.85, 0.005, useUtime = False) #Initilization example line
    testLen = 100
    freq = 1
    xTest = [100*sin(freq*i) for i in range(0, testLen)]
    tTest = [i + 1 for i in range(0, testLen)]
    
    xOutput = []
    vOutput = []
    for i in range(0, testLen):
        (x, v) = abTest.update(xTest[i], tTest[i]) #Update example line
        xOutput.append(x)
        vOutput.append(v)
    
    plt.plot(tTest, xOutput, tTest, xTest)
    plt.legend(['filtered x', 'measured x'])
        