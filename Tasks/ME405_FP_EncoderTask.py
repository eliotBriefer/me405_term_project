"""@file    ME405_FP_EncoderTask.py

@brief      Instantiates encoders in the pitch and roll directions
@details    Specifies appropriate pins and timers per encoder. Filters the encoder
            positions and velocity data using an alpha-beta filter.
            \n
            <B>source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/Tasks/ME405_FP_EncoderTask.py
@author     Nicholas Greco
@date       6/11/2021
"""

import pyb
from ME405_FP_Encoder import ME405_FP_Encoder
import utime
from ME405_FP_Shares import thetaXShare, thetaXDotShare, thetaYShare, thetaYDotShare, t_sample, encTimeQueue
from ME405_FP_Shares import thetaXQueue, thetaXDotQueue, thetaYQueue, thetaYDotQueue, dataClct, enabledShare
from task_share import Share, Queue 
from ME405_FP_ABFilter import ABFilter

# Queue: Buffer or collection to be written and distilled 
#   Ex: the string of user inputs in the Simon Says game
# Share: Single value at any one time
#   Ex: incremental positional data for the encoder

def encoderTaskFun ():
    """ @brief   Instantiates, captures, and filters encoder data
        @details This function implements Task 2, a task which is somewhat
                 sillier than Task 1 in that Task 2 won't shut up. Also, 
                 one can test the relative speed of Python string manipulation
                 with memory allocation (slow) @a vs. that of manipulation of 
                 bytes in pre-allocated memory (faster).
    """
    # Encoder #1
    tim4 = pyb.Timer(4, period = 0xffff, prescaler = 0) 
    pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
    pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
    
    # Encoder #2
    tim8 = pyb.Timer(8, period = 0xffff, prescaler = 0)
    pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
    pinC7 = pyb.Pin(pyb.Pin.cpu.C7)
    
    # Instantiating the Encoders
    encY = ME405_FP_Encoder(tim4, pinB6, pinB7)
    encX = ME405_FP_Encoder(tim8, pinC6, pinC7)
    
    # Instantiating the AlphaBeta Filter which will be used to compute velocities
    ABmotion_encY = ABFilter(0.85, 0.5, 10**6, t_not = utime.ticks_us(), useUtime = True) # Instantiate AB Filter
    ABmotion_encX = ABFilter(0.85, 0.5, 10**6, t_not = utime.ticks_us(), useUtime = True) # Instantiate AB Filter
    
    time_init = utime.ticks_ms()
    time_vec = [time_init]
    
    rm = 0.06 #Radius of lever arm in m
    lp = 0.110 #horizontal distance from U-Joint to push-rod pivot in m

    while True:
        # TAKING A GUESS AS TO WHICH ENCODER CORRESPONDS WITH X-ROTATION AND 
        # WHICH CORRESPONDS WITH Y-ROTATION
        if enabledShare.get() == 1:
            time_init = utime.ticks_ms()            
            
       ################## Encoder #1 Position and velocity ##################
           
            rawYPos = encY.update()*(-1*rm/lp)
            
            # The update() method of the ABFilter class utilizes the live data
            # to provide position and velocity updates for the rotation of both
            # the "X" and "Y" direction encoders
            
            #Filter encoder readings using AB filter
            #Next, convert encoder readings into platform orientation
            #Note: x encoder corresponds to Y plate rotation and y encoder
            #corresponds to x plate rotation
            (th_X, om_X) = ABmotion_encY.update(rawYPos, utime.ticks_us())
            th_X *= (-1*rm/lp)
            om_X *= (-1*rm/lp)
            
            # Share for Encoder #1 Angular Velocity
            thetaXDotShare.put(om_X)
            # Share for Encoder #1 Angular Position
            thetaXShare.put(th_X) 
            
             # Checking to see if the queue is full, and adding position values if not
            if (not thetaXQueue.full()) and dataClct.get() == 1:
                thetaXQueue.put(th_X)

            
            # Filling the queue for angular velocity in the x-direction
            if (not thetaXDotQueue.full()) and dataClct.get() == 1:
                thetaXDotQueue.put(om_X) # values in ticks/ms
            
            # print("{},{},{}".format(th_X, thetaXShare.get(), utime.ticks_ms()))
            # print("Theta_X: {}, Omega_X: {}".format(thetaXShare.get(), thetaXDotShare.get()))
            # print("Theta_Y: {}, Omega_Y: {}".format(thetaYShare.get(), thetaYDotShare.get()))
            
        ################## Encoder Time Queue ##################
       
            # encTimeQueue.put(utime.ticks_ms())

        ################## Encoder #2 Position and velocity ##################
           
        
            rawXPos = encX.update()*(-1*rm/lp)
            
            #Filter encoder readings using AB filter
            #Next, convert encoder readings into platform orientation
            #Note: x encoder corresponds to Y plate rotation and y encoder
            #corresponds to x plate rotation
            (th_Y, om_Y) = ABmotion_encX.update(rawXPos, utime.ticks_us())
            th_Y *= (-1*rm/lp)
            om_Y *= (-1*rm/lp)

            # Share for Encoder #1 Angular Velocity
            thetaYDotShare.put(om_Y)
            # Share for Encoder #1 Angular Position
            thetaYShare.put(th_Y) 
            
             # Checking to see if the queue is full, and adding position values if not
            if (not thetaYQueue.full()) and dataClct.get() == 1:
                thetaYQueue.put(th_Y)
            
            # Filling the queue for angular velocity in the x-direction
            if (not thetaYDotQueue.full()) and dataClct.get() == 1:
                thetaYDotQueue.put(om_Y) # values in ticks/ms
            
            # print("{},{},{}".format(th_X, thetaXShare.get(), utime.ticks_ms()))
            # print("Theta_X: {}, Omega_X: {}".format(thetaXShare.get(), thetaXDotShare.get()))
            # print("Theta_Y: {}, Omega_Y: {}".format(thetaYShare.get(), thetaYDotShare.get()))
        yield (0) 
        
    
