"""@file    ME405_FP_UserTask.py

@brief      Interprets a "go" signal, builds a CSV, & sends it to the FrontEnd
@details    Upon receiving a certain signal from the FrontEnd, the UI_Task FSM
            switches states to begin plugging in a set of times to an arbitrary
            function in order to generate an output vector. Upon reaching 
            3000s, the data is then packaged as a CSV file and written to
            the FrontEnd using serial communication.
            \n
            <B>source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/Tasks/ME405_FP_UserTask.py
@author     Nicholas Greco
@date       4/04/2021
"""

## SOFTWARE HANDSHAKE
from pyb import USB_VCP
from ME405_FP_Shares import enabledShare, dataClct, programExitShare, resetShare

def userInterfaceFun():
    ''' @brief Sets the USB_VCP method to read mode
        @details The user needs to be able to make a variety of choices regarding 
                 the operation of the balancing platform.
                 Those options are specified below:
        * b (Begin Balancing)
        * h (Stop Collecting Data & end the program)
        * d (Collect Data)
        * e (Exit Program)
        * r (Reset Motor Fault)
        * x (Stop Collecting Data & continue the Program)
        
        Entering any of these values into the PuTTY console will cause the 
        corresponding action in the program.
    
    '''
    userCmd = USB_VCP()
    userCmd.init(flow = USB_VCP.RTS)
    print('Enter desired key values')
    
    while True:    
        if userCmd.any():
            ## @brief Decodes commands passed from PuTTY
            init_char = userCmd.readline().decode('utf-8')
            if init_char == "b":
            # If receives a 'b', begin balancing the platform
                enabledShare.put(1)
                print("I received a " + str(init_char))
                print(enabledShare.get())
                
            elif init_char == "h":
                enabledShare.put(0)
                dataClct.put(0)
                print("I received a" + str(init_char))
                print(enabledShare.get())
            
            # Cancels Data Collection
            elif init_char == "x":
                dataClct.put(0)
                print("I received a" + str(init_char))
                print(enabledShare.get())
                
            elif init_char == "d":
                dataClct.put(1)
                print("I received a" + str(init_char))
                print(dataClct.get())
                
            elif init_char == "e":
                programExitShare.put(1)
                print("I received a" + str(init_char))
                print(programExitShare.get())
                
            elif init_char == "r":
                resetShare.put(1)
                print("I received a " + str(init_char))
                print(resetShare.get())
        yield(0)
            
            
#==================Depreciated Class implementation ========================
"""class UserTask:
    '''@brief Performs a data collection, then passes the data to the FrontEnd
       @details Upon receiving a certain signal, the control loop uses the 
                extend method to take the data for time and velocity from 
                SharesLib, then sends that data via serial communication to the 
                FrontEnd, where it can be graphed.
    '''    
    # pyb.repl_uart(None)
    # Above line intended to prevent print statements from being displayed
    # in the UART
    
    def __init__(self):
        ## @brief Sets up the USB_VCP port through which the serial comms occur
        # initializing the mode of the USB_VCP
        self.userCmd = USB_VCP()
        self.userCmd.init(flow = USB_VCP.RTS)
    
    def run(self):
        '''@brief Triggers the data processing upon receiving the "go" prompt
        '''
        while True:    
            if self.userCmd.any() != 0:
                ## @brief Decodes the FrontEnd comms
                self.init_char = self.userCmd.readline().decode('utf-8')
                if self.init_char == "b":
                # If receives a 'b', begin balancing the platform
                    ## @brief Decodes the Kp passed from the FrontEnd
                    startBal.put(1)
                    
                elif self.init_char == "h":
                    userHalt.put(1)
                    
                elif self.init_char == "d":
                    dataClct.put(1)
                
                yield(0)
                
"""
    
        
        
        
        
        
        
        
        
        
 