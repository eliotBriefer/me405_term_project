"""
@file ME405_FP_MotorTask.py
@brief This module is a FSM implementation of the motor task used to power the two dc motors.
@details The motor task recieves duty cycles for each motor from the @ref ME405_FP_ControllerTask "controller task" and 
enables and disables the motor board when the @ref ME405_FP_UserTask.py "user task" updates ME405_FP_Shares.enabledShare.
The motor task is structured as a finite state machine so that it can handle input from the user
task as well as handling motor faults. The state transition diagram is shown below:
\n
\n
<B>source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/Tasks/ME405_FP_MotorTask.py
\n
@image html motorTask.png width=50%
@author Eliot Briefer
@date 6/9/2021
"""

import ME405_FP_DRV8847 as motorDriver
import pyb
from ME405_FP_Shares import xDutyCycleShare, yDutyCycleShare, resetShare, enabledShare
   

def motorTaskFun():
    '''
    @brief This generator function is an FSM implementation of the motor task used to control the motors.
    @details This task uses two @ref ME405_FP_DRV8847.py "motor driver" objects to set the
    duty cycle of the x and y motors using PWM. The duty cycle of each motor is a
    shared variable that is updated by the @ref ME405_FP_ControllerTask.py "controller task" and read by this task.
    @return FSM state
    '''
    
    #@brief This generator function is an FSM implementation of the motor task
    #used to control the motors.
    #@details This task uses two ME405_FP_DRV8847 motor driver objects to set the
    #duty cycle of the x and y motors using PWM. The duty cycle of each motor is a
    #shared variable that is updated by ME405_FP_ControllerTask and read by this task.
    #@return FSM state

    #Initialize Pins
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, mode=pyb.Pin.OUT_PP, value=1);
    pin_nFAULT = pyb.Pin(pyb.Pin.cpu.B2, mode=pyb.Pin.IN)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5);
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0);
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1);
    #Initialaize Timer
    tim3 = pyb.Timer(3, freq = 20000);
    #Initialize board and motor drivers
    ##@brief The ME405_FP_DRV8847.DRV8847 object responsible for enabling and disabling the motors and fault handling.
    motorBoard = motorDriver.DRV8847(pin_nSLEEP, pin_nFAULT)
    ##@brief The ME405_FP_DRV8847.DRV8847_channel oject use to control the y motor.
    #@details The y motor rotates the platform about the x axis, resulting in ball motion along the y axis.
    yMotor = motorBoard.channel(pin_IN1, 1, pin_IN2, 2, tim3)
    ##@brief The ME405_FP_DRV8847.DRV8847_channel oject use to control the x motor.
    #@details The y motor rotates the platform about the y axis, resulting in ball motion along the x axis.
    xMotor = motorBoard.channel(pin_IN3, 3, pin_IN4, 4, tim3)
    
    ##@brief The state of the motor task FSM.
    state = 0


    while True:
        if state == 0:
            #S0: Motors disabled, waiting"
            if motorBoard.fault:
                motorBoard.disable()
                state = 2
                pass
            elif enabledShare.get() == 1:
                motorBoard.enable()
                state = 1
            else:
                motorBoard.disable()
                
        elif state == 1:
            #S1: Enabled, motors updating duty cycle
            if motorBoard.fault:
                motorBoard.disable()
                state = 2
            if enabledShare.get() == 1:
                yMotor.set_level(yDutyCycleShare.get())
                xMotor.set_level(xDutyCycleShare.get())
            else:
                yMotor.set_level(0)
                xMotor.set_level(0)
                state = 0
            
        elif state == 2:
            #S2: Motor board fault
            if resetShare.get() == 1:
                resetShare.put(0)
                if motorBoard.clear_fault(): #motorBoard.clear_fault() returns True if fault cleared succsessfully
                    state = 0
        else:
            raise ValueError ('Illegal state for motor task')

        yield (state)

