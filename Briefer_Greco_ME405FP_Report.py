## @page me405FP ME405 FINAL PROJECT       
# @author     Eliot Briefer and Nicholas Greco
# @date       6/11/2021
#
# @section sec_Description Project Overview
#
# The ME 405 term project serves to demonstrate skill with 
# multi-tasking, mechanical modeling, control theory, and firmware implementation.
# This takes the form of using these skills to balance an inverted pendulum
# system, in our case a platform capable of pitch and roll motion.
# The following documentation includes the drivers, tasks, main file, 
# graphs, and diagrams. Taken together, these demonstrate the underlying 
# mechanisms of platform operation, the means of operating said platform,
# the architecture of our code, and the results upon balancing the platform.
# We have discussed our design process and results in detail below, or for a general
# overview we have also provided a video overview:
#\n
#  * Demonstration Video: https://www.youtube.com/watch?v=3qtcu8_9CDg&ab_channel=NicholasGreco
#  * Source Code: https://bitbucket.org/eliotBriefer/me405_term_project/src/master/
#
# @section sec_CodeLayout Code Layout
#
# The core functionality of our code is divided up into tasks that
# allow us to perform multitasking on the single threaded STM32 microchip.
# Each task is responsible for one major element of functionality. Below
# is a list of links to the documentation for each task:
# \n
#  * @ref ME405_FP_UserTask.py "User Task": reads keyboard input from a connected PC.
#  * @ref ME405_FP_EncoderTask.py "Encoder Task": reads the position of both encoders.
#  * @ref ME405_FP_TouchPanelTask.py "Touch Panel Task": reads the position of the ball from the touch panel.
#  * @ref ME405_FP_ControllerTask.py "Controller Task": calculates how to actuate each motor to balance the ball.
#  * @ref ME405_FP_MotorTask.py "Motor Task": updates the duty cycle of each motor.
#  * @ref ME405_FP_DataTask.py "Data Collection Task": logs system data to a csv file on the Nucleo.
#  .
# \n
# While each task is responsible for communicating with other tasks, we also designed driver classes which are
# responsible for directly communicating with the hardware:
# \n
#  * @ref ME405_FP_Encoder.py "Encoder Driver"
#  * @ref ME405_FP_ResTP.py "Touch Panel Driver"
#  * @ref ME405_FP_DRV8847.py "Motor Board and Motor Driver"
#  .
# \n
# There are also a few modules that aren't tasks or drivers that are necessary for our program to function:
# \n
#  * @ref main.py "Main module": schedules and runs all tasks.
#  * @ref ME405_FP_Shares.py "Shares Module": initializes all shared variables for data sharing between tasks.
#  * @ref ME405_FP_FSC.py "Full State Controller": a general full state controller class for full state feedback with a single output.
#  * @ref ME405_FP_ABFilter.py "Alpha Beta Filter": a general alpha beta filter class for better velocity estimation from granular data.
#  .
#
# @section sec_TaskDiagram Task Communication
#
# Because each task only handles one seciont of the overall program, the tasks need to share data with
# eachother. The task diagram shown bellow illustrates how data is shared between classes. Each task is
# designed to run at a specified period and with a specified priority. Tasks with higher priority are run
# before tasks with a lower priority if both tasks are requesting to be run.
# \n
# @image html ME405_FP_taskDiagram.png
# \n
# Data sharing is achieved by creating cotask.Share and cotask.Queue objects in the @ref ME405_FP_Shares.py "shares module".
# Each task then imports the shares and queues that they need to function so that they can read and right to the shares and queues.
# 
#@section sec_velocityEstimation Velocity Estimation with Alpha Beta Filters
# When dealing with a system near zero velocity, it is common to run in to problems estimating velocities. This is most often due to sensors,
# such as the encoders or touch panel having too low a resolution to smoothly resolve very slow displacements, which results in granular position
# data. Granular position data can cause the velocity to spike when, for instance, an encoder registers a new tick. To avoid these velocity spikes
# we implemented an alpha beta velocity estimation filter which acts similar to a low pass filter and eliminates sudden spikes in velocity.
# The goal of an alpha beta filter is to have the filtered velocity track closely with the "naive" velocity estimation method, but eliminate sharp
# peaks in estimated velocity. Shown below are graphs of the "naive" and filtered velocities measured by the encoders, as well as the "naive"
# and filtered velocities measured by the touch panel.
#
# @image html TouchPanel_AB.png
# @image html EncoderTesting_AB.png
#
# We generated these graphs by moving the platform by hand and moving a finger around the touch panel.
# We used these graphs to adjust the values of alpha and beta for both the touch panel and
# the encoders until the filtered velocity had minimal phase shift relative to the "naive" velocity estimation but still eliminated the peaks
# from the "naive" velocity.
#
#
# @section secControllerDesign Full State Controller Design
# The key to sucsessfully balancing the ball on the platform is a well designed controller. In order to analytically design a full
# state controller we needed to fist create a dynamic system model, which can be found @ref systemModel "here". Next, we linearized the system
# at the opperating point, shown @ref HW0x04 "here". Finally, we used pole placement to find the 
# controller gains for our full state controller, shown @ref HW0x05 "here".
# \n
# Once all of our tasks were functioning we used these gains as a starting point for tuning our controller. We first started by trying to
# balance the platform in a horizontal position without the ball on it by only adjusting the controller gains for the platform angular position 
# and velocity. We repeatedly doubled the angular velocity gains untill the platform begane to oscillate. Then we increased the angular velocity
# gain until the oscillation was significantly reduced. Next, we placed the ball on the touch panel and increased the ball position gains until
# the system became unstable and then reduced the position gains slightly. As the nex step, we increased the ball velocity gains until we couldn't
# see any noticable bost in the system performance. Form there we experimented with incrementally increasing and decreasing the ball position and
# velocity gains to see if we could improve the system's performance.
#
# @section secResults Results
# Ultimatley we were able to tune our controller to balance the ball near the center of the platform if the system started at zero initial conditions.
# However, if the ball was placed on the platform off center or we pushed the ball with a finger the platform would begin to oscillate and the system
# would become unstable. The graphs below show the angular postion and velocity of the plate as it is balancing the ball.
# \n
#  * Link to video showing results: https://www.youtube.com/watch?v=3qtcu8_9CDg&ab_channel=NicholasGreco
#  .
# \n
# @image html resultsPosition.png
# \n
# @image html velocityPlot.png
# \n

##  @page HW0x04  Simulation or Reality?
#  
#  @section sec_hw0x04_summary Summary
#  Building off of HW 0x02, we are furthering our preparation for our term project by developing a simulation and test a controller for the simplified
#  model of the pivoting platform. Our group decided to attempt both python and matlab to complete this assignment, with the resulting and properly functioning
#  submission to be in matlab, which you will find below. However, you will find our python script linked below as well. 
# 
#  @section sec_hw0x04_parameters Numerical Parameters
#  In our simuation, we used the following parameters:
#
#  @image html hw0x04Parameters.png "Figure 1. Provided numerical parameters for the simulation"
# 
#  We did not need to design our own controller for this assignment, rather we were given the following gains to test our system performance in closed loop:
#  K = [-0.3, -0.2, -0.05, -0.02] with the units K = [N, N*m, N*s, N*m*s]
#
#  @section sec_hw0x04_engineering_requirments Engineering Requirements
#  - Use the linearization of our system model and express it in state-space form
#  - Using software, implement model that can be simulated in open-loop given a prescribed set of inital conditions
#  - Run simulations in open-loop and generate series of plots showing the dynamic response for each state variable
#  - <b> Simulation A:</b> The ball is initially at rest on a level platform directly above the center of gravity of the platform and 
#    there is no torque input from the motor. Run this simulation for 1 [s].
#  - <b> Simulation B:</b> The ball is initially at rest on a level platform offset horizontally from the center of gravity of the 
#    platform by 5 [cm] and there is no torque input from the motor. Run this simulation for 0.4 [s].
#  - <b> Simulation C:</b> Test simulation in closed-loop by implementing a regulator using full state feedback: u = - Kx and run simulation for at
#    least 20 [s]. 
#
#  @section sec_hw0x04_matlab_simlink Matlab and Simulink Documentation
#  <b> Matlab Script:</b>
#  @image html HW4_SystemParameters.png "Figure 2. System Parameters Matlab Script"
#
#  @image html HW4_SettinguptheMatrices.png "Figure 3. Setting up the Matrices Matlab Script"
#
#  @image html HW4_PlottingResults.png "Figure 4. Plotting Results Matlab Script"
#
#  @image html HW4_DeterminingtheInverse.png "Figure 5. Determining the Inverse Matlab Script"
#
#  @image html HW4_OpenLoopModelCaseA_1.png
#  @image html HW4_OpenLoopModelCaseA_2.png "Figure 6. Simulation/Case A Open Loop Matlab Script"
#
#  @image html HW4_OpenLoopModelCaseB_1.png 
#  @image html HW4_OpenLoopModelCaseB_2.png "Figure 7. Simulation/Case B Open Loop Matlab Script"
#
#  @image html HW4_ClosedLoopModel_1.png 
#  @image html HW4_ClosedLoopModel_2.png "Figure 8. Simulation/Case C Closed Loop using Case B Test Conditions Matlab Script"
#
#  <b> Simulink:</b>
#
#  @image html HW4_SimulinkModel.png "Figure 9. Simulink Model"
#
#  @section sec_hw0x04_plots Simulation Plots
#  <b> [Simulation A] </b>
#  @image html HW4_OL_Plot_PartA.jpeg "Figure 10. Subplots for Simulation/Case A Open Loop"
#
#  <b> Discussion: </b> The ball is initially placed at the center of the platform, the one stable point in our inherently unstable system. 
#  Given that there are no appreciable forces acting on the ball in this simulation, no initial displacements/velocities of the platform or ball, 
#  and no torque input from the motor, this system will not deviate from its initial stable point. Therefore, this is a static system, and 
#  such behavior is shown by the zero slope lines in all displacement and velocity plots.
#  
#  <b> [Simulation B] </b>
#  @image html HW4_OL_Plot_PartB.jpeg "Figure 11. Subplots for Simulation/Case B Open Loop"
#
#  <b> Discussion: </b> The position of the ball as shown in plot [1,1] shows an interesting tendency to climb the incline, moving a direction opposite the tilt. 
#  Given that no force is being put into the ball, this tendency is hard to explain. Using newton’s 2nd law it is conceivable that the ball has an small nonzero 
#  acceleration as the platform tilts that acts in a direction opposite the motion of the platform. This is confirmed when we look at the velocity graph, whose 
#  change with respect to time is distinctly nonlinear and negative, meaning there is a nonzero, negative acceleration in the direction opposite the rotation of the ball. 
#  Frictional effects not being appropriately modeled might also have something to do with this physically inconceivable upward motion.
#  
#  With the oddity explained, the rest of the ball’s position and velocity graph’s show motion we would expect of an open loop controller. The platform will continue to tilt 
#  to greater and greater angles [plot[1,2]] at a higher and higher rate [plot [2,2]], increasing towards instability, the point at which the ball falls off the platform.
#
#  <b> [Simulation C] </b>
#  @image html HW4_CL_Plot.jpeg "Figure 12. Subplots for Simulation/Case C Closed Loop"
#
#  <b> Discussion: </b> As plot[1,1] shows, the ball starts at a position of x = 0.05m, which drives the plate to tilt clockwise about the y-axis, as shown in plot[1, 2]. 
#  The system’s regulator detects this initial displacement and attempts to correct the displacement by driving the motor to oppose the ball’s displacement. 
#  As a result, even though the platform’s angular velocity starts negative [clockwise] owing to the ball’s initial displacement [plot [2,2]], the clockwise 
#  angular velocity slowly decreases as the torque from the motor opposes the ball’s translation and, by association, the clockwise angular displacement of the platform as well. 
#  The effect on the ball’s velocity is shown in plot[2,1], where the counteracting torque applied by the motor to the platform drives the ball’s position to change in the direction 
#  opposite its initial displacement.
#
#  The above description is for one cycle of the closed loop controller. Owing to mechanical losses and the corrective actions of the regulator, the peaks associated with each oscillation 
#  will gradually decrease until the ball is more or less returned to a stable state at the center of the platform, which occurs roughly 20 seconds after the initial linear displacement.


## @page HW0x05 Pole Placement
#  @section sec_hw0x05_summary Summary
#  In preparation of our term project and to continue to build off of HW 0x02 and HW 0x04, we developed a closed loop controller
#  for our system model. To ensure consistency and accuracy of our system, we decided to pursue the MatLab/Simulink tools in order to 
#  complete our assignment. You will find our Matlab and Simulink documentation below.
#
#  @section sec_hw0x05_engineering_requirements Engineering Requirements
#  
#  * Determine 1x4 gain vector K = [K1, K2, K3, K4]
#  * Define a method of determining pole locations based criteria
#  * Find the characteristic polynomial associated with selected pole locations
#  * Determine the gains K1 through K4.
#
#  Method of determining pole locations based on criteria was making a dominant pole approximation and choosing two large poles along with two 
#  time-domain characteristics like settling time and overshoot percentage. 
#
#  @section sec_hw0x05_Hand_Calculations Hand Calculations
#  @image html hw0x05_handcals_1.png
#  @image html hw0x05_handcals_2.png
#
#  @section sec_hw0x05_matlab_and_simulink Matlab and Simulink
#
#  <b> HW 0x05: Developing a Closed Loop Control System for the Balancing Platform</b>
#  
#  @image html hw0x05_systemparameters.png 
#  @image html hw0x05_1.png
#  @image html hw0x05_2.png
#  @image html hw0x05_3.png
#  @image html hw0x05_4.png
#  @image html hw0x05_6.png
#  @image html hw0x05_7.png
#
#  @image html HW4_SimulinkModel.png "Figure 1. Simulink Model"
#  
#  @section sec_hw0x05_Results Results
#  <b> Verify K values using place() command </b>
#  @image html hw0x05_verify.png "Figure 2. Compared calculated results versus results found via place() command"
#  
#  As you can see in Figure 1, the values we outputted matched that of the values resulting from the use of the place() command.
#
#  @image html HW0x04_Controller.jpeg "Figure 3. HW 0x04 Controller Plots"
#
#  @image html OurController.jpeg "Figure 3. HW 0x05 Controller Plots"
#  
#  @section sec_hw0x05_discussion Discussion & Analysis
#  <b> 1. Show confirmation that your calculated gains correctly place the closed-loop poles at your desired locations </b>
#  
#  Setting and iterating upon the parameters of rise time and percent overshoot, we arrive at gain values after solving the 
#  system characteristic polynomial for the values of "K". When using the place() function to compare the calculated gains 
#  to that calculated using the place() function (see attached MATLAB code), we see identical results between the two, 
#  with the values being: [-1.7159 -0.6088 -0.4783 -0.547].
#
#  <b> 2. Show your tuned closed-loop simulation results and comment on the differences (hopefully positive ones) compared to the
#  closed-loop response in HW 0x04. </b>
#  
#  From inspection, the tuned open-loop system has a smaller settling time, rise time and percent overshoot. Accomplishing such 
#  variations involved increasing the value of the damping ratio (zeta) which increased the value of the real term, driving percent 
#  overshoot and settling time down. Additionally, but to a lesser extent, the increase of the damping ratio and increase of the 
#  natural frequency drove the rise time up as well. These differences illustrate a more effective controller, one that reaches 
#  equilibrium sooner, that responds to the ball's placement quicker, and exhibits more controlled motion.
#
#  <b> 3. Comment on the performance of your system: Did you achieve the criteria that you selected? </b>
#  
#  In order to compare percent overshoot values between the HW0x04 controller and the HW0x05 controller design above, 
#  I used the min function to obtain minimum (negative) values as these correspond to the actual peak. The HW0x04 
#  controller exhibited a peak of -0.03048 [m], whereas the HW0x05 controller exhibited a peak of -0.0068 [m]. 
#  Given the large difference between the values and that they both share the same steady state value, the explicit 
#  OS comparison is unnecessary. Additionally, using the damping ratio and natural frequency values of the HW0x05 
#  controller, we get a settling time of 1.195 [s], which can be observed as being far less than that of the HW0x04 
#  controller. Finally, using peak time as opposed to rise time calculations because they both would exhibit similar 
#  trend (and for the sake of parsimony), we find the peak time of the HW0x05 controller to be 0.5638 [s], as compared 
#  to the 1.142 [s] value of the HW0x04 controller.

